@extends("layouts.default")

@section('title', 'Page d\'accueil')

@section('contenu')
<section id="main"> 
    <h1>Welcome</h1>
<p>Bienvenue sur cette magnifique page d'accueil du site</p>
@if (isset($result))
    <p>Le résultat de la multiplication est : {{$result}}</p>
@endif
</section>
@endsection  